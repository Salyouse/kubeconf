module mod

go 1.22.3

require (
	github.com/olekukonko/tablewriter v0.0.5
	gitlab.com/Salyouse/kubeconf v0.0.2
)

require (
	github.com/mattn/go-runewidth v0.0.9 // indirect
	gopkg.in/yaml.v2 v2.4.0 // indirect
)
